package com.example.christinaplemmenou.cs_akazoo_app.tracks;

public interface OnTracksClickListener {

    void OnTrackClicked(Track track);

    void OnTrackLogoClick(Track track);

}
