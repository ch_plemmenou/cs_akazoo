package com.example.christinaplemmenou.cs_akazoo_app.tracks;

public class Track {
    private String trackName;
    private String trackArtist;
    private String trackCategory;

    private String trackLogoUrl;

    public Track(String trackName, String trackArtist, String trackCategory) {
        this.trackName = trackName;
        this.trackArtist = trackArtist;
        this.trackCategory = trackCategory;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getTrackArtist() {
        return trackArtist;
    }

    public String getTrackCategory() {
        return trackCategory;
    }

    public String getTrackLogoUrl() {
        return trackLogoUrl;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public void setTrackArtist(String trackArtist) {
        this.trackArtist = trackArtist;
    }

    public void setTrackCategory(String trackCategory) {
        this.trackCategory = trackCategory;
    }

    public void setTrackLogoUrl(String trackLogoUrl) {
        this.trackLogoUrl = trackLogoUrl;
    }
}
