package com.example.christinaplemmenou.cs_akazoo_app.playlists;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.christinaplemmenou.cs_akazoo_app.R;

import java.util.ArrayList;

public class PlaylistsRvAdapter extends RecyclerView.Adapter<PlaylistsRvAdapter.PlaylistsViewHolder> {

    private ArrayList<Playlist> playlists;
    private OnPlaylistClickListener listener;

    public PlaylistsRvAdapter(ArrayList<Playlist> playlists, OnPlaylistClickListener listener) {
        this.playlists = playlists;
        this.listener = listener;
    }

    //    public PlaylistsRvAdapter(ArrayList<Playlist> playlists) {
//        this.playlists = playlists;
//    }



    public static class PlaylistsViewHolder extends RecyclerView.ViewHolder{

        TextView mPlayListName;
        TextView mTracksNumber;
        ImageView mPlayListLogo;
        RelativeLayout mPlaylistItemRoot;


        public PlaylistsViewHolder(View itemView) {
            super(itemView);
            mPlayListName = itemView.findViewById(R.id.playlists_name);
            mTracksNumber = itemView.findViewById(R.id.tracks_number);
            mPlayListLogo = itemView.findViewById(R.id.playlist_logo);
            mPlaylistItemRoot = itemView.findViewById(R.id.playlist_item_root);
        }
    }

    @NonNull
    @Override
    public PlaylistsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_playlist_item,parent,false);

        PlaylistsViewHolder vh = new PlaylistsViewHolder(v);

        return vh;
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistsViewHolder holder, final int position) {
        final int final_position = position;
        holder.mPlayListName.setText(playlists.get(final_position).getName());
        holder.mTracksNumber.setText(String.valueOf(playlists.get(final_position).getItemCount()));
        holder.mPlayListLogo.setImageResource(R.mipmap.ic_launcher);

        holder.mPlaylistItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onPlaylistClicked(playlists.get(final_position));
            }
        });

    }
}
