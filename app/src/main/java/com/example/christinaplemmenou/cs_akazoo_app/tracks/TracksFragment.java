package com.example.christinaplemmenou.cs_akazoo_app.tracks;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.christinaplemmenou.cs_akazoo_app.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TracksFragment extends Fragment implements TracksView{

    RecyclerView tracksRv;
    TracksPresenter tracksPresenter;

    public TracksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tracks, container, false);

        tracksRv = v.findViewById(R.id.tracks_rv);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        tracksRv.setLayoutManager(layoutManager);

        tracksPresenter = new TracksPresenterImpl(this);
        tracksPresenter.getTracks();



        return v;
    }



    @Override
    public void showTracks(ArrayList<Track> tracklist) {

        TracksRvAdapter tracklistRvAdapter = new TracksRvAdapter(tracklist, new OnTracksClickListener() {
            @Override
            public void OnTrackClicked(Track track) {
                Toast.makeText(getActivity(),"Track "+ track.getTrackName()+" got clicked",Toast.LENGTH_LONG).show();
            }

            @Override
            public void OnTrackLogoClick(Track track) {
                Toast.makeText(getActivity(),"Track logo "+ track.getTrackName()+" got clicked",Toast.LENGTH_LONG).show();
            }
        });
        tracksRv.setAdapter(tracklistRvAdapter);
    }
}
