package com.example.christinaplemmenou.cs_akazoo_app.tracks;

import java.util.ArrayList;

public interface TracksView {

    void showTracks(ArrayList<Track> tracks);

}
