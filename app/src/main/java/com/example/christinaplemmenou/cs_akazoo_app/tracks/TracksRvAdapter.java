package com.example.christinaplemmenou.cs_akazoo_app.tracks;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.christinaplemmenou.cs_akazoo_app.R;

import java.util.ArrayList;

public class TracksRvAdapter extends RecyclerView.Adapter<TracksRvAdapter.TrackListViewHolder>{

    private ArrayList<Track> tracksList;
    private OnTracksClickListener mListener;

    public TracksRvAdapter(ArrayList<Track> tracksList, OnTracksClickListener mListener) {
        this.tracksList = tracksList;
        this.mListener = mListener;
    }

    //    public TracksRvAdapter(ArrayList<Track> tracksList) {
//        this.tracksList = tracksList;
//    }

    public static class TrackListViewHolder extends RecyclerView.ViewHolder{

        TextView mTrackName;
        TextView mTrackAuthor;
        TextView mTrackCategory;
        ImageView mTrackLogo;
        RelativeLayout mTrackItemRoot;


        public TrackListViewHolder(View itemView) {
            super(itemView);
            mTrackName = itemView.findViewById(R.id.track_name);
            mTrackAuthor = itemView.findViewById(R.id.track_author);
            mTrackCategory = itemView.findViewById(R.id.track_category);
            mTrackLogo = itemView.findViewById(R.id.tracks_logo);
            mTrackItemRoot = itemView.findViewById(R.id.track_item_root);
        }
    }

    @NonNull
    @Override
    public TrackListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_tracks_item,parent,false);

        TrackListViewHolder vh = new TrackListViewHolder(v);

        return vh;
    }

    @Override
    public int getItemCount() {
        return tracksList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull TrackListViewHolder holder, int position) {
        final int final_position = position;
        holder.mTrackName.setText(tracksList.get(final_position).getTrackName());
        holder.mTrackAuthor.setText(tracksList.get(final_position).getTrackArtist());
        holder.mTrackCategory.setText(tracksList.get(final_position).getTrackCategory());
        holder.mTrackLogo.setImageResource(R.mipmap.ic_launcher);
        holder.mTrackLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.OnTrackLogoClick(tracksList.get(final_position));
            }
        });
        holder.mTrackItemRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.OnTrackClicked(tracksList.get(final_position));
            }
        });

    }

}
