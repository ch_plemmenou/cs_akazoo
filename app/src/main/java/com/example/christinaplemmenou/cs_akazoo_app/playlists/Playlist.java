package com.example.christinaplemmenou.cs_akazoo_app.playlists;

public class Playlist {

    private String playlistID;
    private String name;
    private int itemCount;
    private String photoUrl;

    public Playlist(String playlistID, String name, int itemCount) {
        this.playlistID = playlistID;
        this.name = name;
        this.itemCount = itemCount;
    }

    public Playlist(String playlistID, String name, int itemCount, String photoUrl) {
        this.playlistID = playlistID;
        this.name = name;
        this.itemCount = itemCount;
        this.photoUrl = photoUrl;
    }

    public void setPlaylistID(String playlistID) {
        this.playlistID = playlistID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPlaylistID() {
        return playlistID;
    }

    public String getName() {
        return name;
    }

    public int getItemCount() {
        return itemCount;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
