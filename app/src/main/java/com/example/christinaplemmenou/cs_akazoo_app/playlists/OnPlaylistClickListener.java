package com.example.christinaplemmenou.cs_akazoo_app.playlists;

public interface OnPlaylistClickListener {

    void onPlaylistClicked(Playlist playlist);

}
