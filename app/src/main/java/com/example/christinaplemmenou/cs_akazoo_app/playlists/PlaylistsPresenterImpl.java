package com.example.christinaplemmenou.cs_akazoo_app.playlists;

import java.util.ArrayList;

public class PlaylistsPresenterImpl implements PlaylistsPresenter {

    PlaylistsView playlistsView;

    public PlaylistsPresenterImpl(PlaylistsView playlistsView) {
        this.playlistsView = playlistsView;
    }

    @Override
    public void getPlaylists() {
        playlistsView.showPlaylists(insertMockPlaylists());
    }

    private ArrayList<Playlist> insertMockPlaylists() {

        ArrayList<Playlist> playlists = new ArrayList<Playlist>();
        playlists.add(new Playlist("1","Nisiotika",20));
        playlists.add(new Playlist("2","Rock",14));
        playlists.add(new Playlist("3","Pop",9));
        playlists.add(new Playlist("4","Metal",3));
        playlists.add(new Playlist("1","Nisiotika",20));
        playlists.add(new Playlist("2","Rock",14));
        playlists.add(new Playlist("3","Pop",9));
        playlists.add(new Playlist("4","Metal",3));
        playlists.add(new Playlist("1","Nisiotika",20));
        playlists.add(new Playlist("2","Rock",14));
        playlists.add(new Playlist("3","Pop",9));
        playlists.add(new Playlist("4","Metal",3));
        playlists.add(new Playlist("1","Nisiotika",20));
        playlists.add(new Playlist("2","Rock",14));
        playlists.add(new Playlist("3","Pop",9));
        playlists.add(new Playlist("4","Metal",3));
        playlists.add(new Playlist("1","Nisiotika",20));
        playlists.add(new Playlist("2","Rock",14));
        playlists.add(new Playlist("3","Pop",9));
        playlists.add(new Playlist("4","Metal",3));
        playlists.add(new Playlist("1","Nisiotika",20));
        playlists.add(new Playlist("2","Rock",14));
        playlists.add(new Playlist("3","Pop",9));
        playlists.add(new Playlist("4","Metal",3));

        return playlists;
    }
}
