package com.example.christinaplemmenou.cs_akazoo_app.playlists;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.christinaplemmenou.cs_akazoo_app.R;
import com.example.christinaplemmenou.cs_akazoo_app.tracks.TracksActivity;
import com.example.christinaplemmenou.cs_akazoo_app.tracks.TracksFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaylistFragment extends Fragment implements PlaylistsView{

    RecyclerView playlistsRv;
    PlaylistsPresenter presenter;

    public PlaylistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_playlist, container, false);

        playlistsRv = v.findViewById(R.id.playlists_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        playlistsRv.setLayoutManager(layoutManager);

        presenter = new PlaylistsPresenterImpl(this);
        presenter.getPlaylists();

//        ArrayList<Playlist> playlists = new ArrayList<>();
//        insertMockPlaylists(playlists);



        return v;
    }




    @Override
    public void showPlaylists(ArrayList<Playlist> playlists) {
        PlaylistsRvAdapter playlistsRvAdapter = new PlaylistsRvAdapter(playlists, new OnPlaylistClickListener() {
            @Override
            public void onPlaylistClicked(Playlist playlist) {
                Toast.makeText(getActivity(),"The playlist: "+playlist.getName() + " got clicked",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(),TracksActivity.class);
                startActivity(intent);
            }
        });
        playlistsRv.setAdapter(playlistsRvAdapter);
    }
}
