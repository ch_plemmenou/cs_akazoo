package com.example.christinaplemmenou.cs_akazoo_app.base;

import android.app.Application;
import android.util.Log;

public class AkazooApplication extends Application {

    private static AkazooApplication INSTANCE;

    public static AkazooApplication getINSTANCE() {

        if (INSTANCE == null)
            INSTANCE = new AkazooApplication();

        return INSTANCE;

    }

    public AkazooApplication() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("CS_TAG","Application got created");
    }

    public void onTerminate(){
        Log.e("CS_TAG","Application got terminated");
    }
}
