package com.example.christinaplemmenou.cs_akazoo_app.tracks;

import java.util.ArrayList;

public class TracksPresenterImpl implements TracksPresenter{

    TracksView tracksView;

    public TracksPresenterImpl(TracksView tracksView) {
        this.tracksView = tracksView;
    }

    @Override
    public void getTracks() {
        tracksView.showTracks(insertMockTrackList());
    }

    private ArrayList<Track> insertMockTrackList() {
        ArrayList<Track> tracklist = new ArrayList<Track>();
        tracklist.add(new Track("Track 1","Efi Thodi","Nisiotika"));
        tracklist.add(new Track("Track 2","Eleonora","Nisiotika"));
        tracklist.add(new Track("Track 3","Marina","Nisiotika"));
        tracklist.add(new Track("Track 4","Kleopatra","Nisiotika"));
        tracklist.add(new Track("Track 1","Efi Thodi","Nisiotika"));
        tracklist.add(new Track("Track 2","Eleonora","Nisiotika"));
        tracklist.add(new Track("Track 3","Marina","Nisiotika"));
        tracklist.add(new Track("Track 4","Kleopatra","Nisiotika"));
        tracklist.add(new Track("Track 1","Efi Thodi","Nisiotika"));
        tracklist.add(new Track("Track 2","Eleonora","Nisiotika"));
        tracklist.add(new Track("Track 3","Marina","Nisiotika"));
        tracklist.add(new Track("Track 4","Kleopatra","Nisiotika"));
        tracklist.add(new Track("Track 1","Efi Thodi","Nisiotika"));
        tracklist.add(new Track("Track 2","Eleonora","Nisiotika"));
        tracklist.add(new Track("Track 3","Marina","Nisiotika"));
        tracklist.add(new Track("Track 4","Kleopatra","Nisiotika"));
        tracklist.add(new Track("Track 1","Efi Thodi","Nisiotika"));
        tracklist.add(new Track("Track 2","Eleonora","Nisiotika"));
        tracklist.add(new Track("Track 3","Marina","Nisiotika"));
        tracklist.add(new Track("Track 4","Kleopatra","Nisiotika"));

        return tracklist;

    }
}
